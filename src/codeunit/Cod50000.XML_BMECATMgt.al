codeunit 50000 "XML_BMECATMgt"
{
    trigger OnRun();
    begin

    end;

    procedure ExportBMECAT(RequestWebshopCode: Code[250]; RequestWebshopPublish: Boolean; RequestItemNo: Code[50]);
    var
        XMLDOMMgmt: Codeunit "fav - XML DOM Mgt.";
        XMLDoc: XmlDocument;
        XMLWriteOpt: XmlWriteOptions;
        AttributeValue: Record "Attribute Value";
        Attribute: Record "Attribute";
        ItemCrossReference: Record "Item Cross Reference";
        ItemCategory: Record "Item Category";
        Item: Record Item;
        CurrNode: XmlNode;
        CurrNode2: XmlNode;
        CurrNode3: XmlNode;
        CurrNodeList: XmlNodeList;
        CurrExchRate: Record "Currency Exchange Rate";
        Unit: Decimal;
        DebugFile: File;
        OutStr: OutStream;
        NsMgr: XmlNamespaceManager;
        ArticleMapOrderCounter: Integer;
        Counter: Integer;
        TempBlob: Record TempBlob;
        FileManagement: Codeunit "File Management";
        FileTransportType: Record "File Transport Type";
        ImportExportTransportManagment: Codeunit "Import/Export Management";
        FileTransportManagment: Codeunit "File Transport Management";
        OStream: OutStream;
        IStream: InStream;
        TempText: Text;
        ExtentedTextLines: Record "Extended Text Line";
        DescLong: Text;
        WebshopItem: Record "Webshop - Item";
        Templist: List of [Text];


    begin
        with XMLDOMMgmt do begin

            AddRootElement(XMLDoc, 'BMECAT', CurrNode2);
            SetUtf8Declaration(XMLDoc);
            AddAttribute(CurrNode2, 'Version', '1.2');
            CurrNode := CurrNode2;
            AddElement(CurrNode, 'HEADER', '', '', CurrNode2);
            CurrNode := CurrNode2;
            AddElement(CurrNode, 'GENERATOR_INFO', 'faveo BMECATEXPORT', '', CurrNode2);

            AddGroupNode(CurrNode, 'CATALOG');

            AddElement(CurrNode, 'LANGUAGE', 'deu', '', CurrNode2);
            AddElement(CurrNode, 'CATALOG_ID', '9999', '', CurrNode2);
            AddElement(CurrNode, 'CATALOG_VERSION', '001.001', '', CurrNode2);
            GetParentNode(CurrNode, CurrNode2);
            AddGroupNode(CurrNode2, 'SUPPLIER');
            CurrNode := CurrNode2;
            AddElement(CurrNode, 'SUPPLIER_ID', '', '', CurrNode2);
            AddAttribute(CurrNode, 'Type', 'supplier_specific');
            AddElement(CurrNode, 'SUPPLIER_NAME', 'Apeltrath &amp; Rundt', '', CurrNode2);

            CurrNode := CurrNode2;
            GetRootNode(XMLDoc, CurrNode2);

            AddGroupNode(CurrNode2, 'T_NEW_CATALOG');
            CurrNode := CurrNode2;
            AddGroupNode(CurrNode, 'CATALOG_GROUP_SYSTEM');

            AddElement(CurrNode, 'CATALOG_GROUP_SYSTEM_ID', 'GROUP SYSTEM ID', '', CurrNode2);
            AddElement(CurrNode, 'GROUP_SYSTEM_NAME', 'GROUP SYSTEM NAME', '', CurrNode2);
            // Head Close -> startet aus Artikel Kategorien
            //standard Katalog root erstellen 
            AddGroupNode(CurrNode, 'CATALOG_STRUCTURE');
            AddAttribute(CurrNode, 'type', 'root');
            AddElement(CurrNode, 'GROUP_ID', '1', '', CurrNode2);
            AddElement(CurrNode, 'GROUP_NAME', 'Katalog', '', CurrNode2);
            AddElement(CurrNode, 'PARENT_ID', '0', '', CurrNode2);
            AddElement(CurrNode, 'GROUP_ORDER', '1', '', CurrNode2);

            AddGroupNode(CurrNode, 'MIME_INFO');
            AddGroupNode(CurrNode, 'MIME');
            AddElement(CurrNode, 'MIME_TYPE', '', '', CurrNode2);
            AddElement(CurrNode, 'MIME_SOURCE', '', '', CurrNode2);

            GetParentNode(CurrNode, CurrNode2);
            CurrNode := CurrNode2;
            GetParentNode(CurrNode, CurrNode2);
            CurrNode := CurrNode2;
            GetParentNode(CurrNode, CurrNode2);
            CurrNode := CurrNode2;
            ArticleMapOrderCounter := 2;
            ItemCategory.SetCurrentKey("Presentation Order");
            if ItemCategory.FINDSET() then begin
                repeat
                    AddGroupNode(CurrNode, 'CATALOG_STRUCTURE');


                    AddElement(CurrNode, 'GROUP_ID', ItemCategory.Code, '', CurrNode2);
                    AddElement(CurrNode, 'GROUP_NAME', ItemCategory.Description, '', CurrNode2);
                    case ItemCategory.Indentation of
                        0:
                            begin
                                AddAttribute(CurrNode, 'type', 'node');
                                ArticleMapOrderCounter := 2;
                                AddElement(CurrNode, 'PARENT_ID', '1', '', CurrNode2);
                            end;
                        else begin
                                AddAttribute(CurrNode, 'type', 'leaf');
                                ArticleMapOrderCounter += 1;
                                AddElement(CurrNode, 'PARENT_ID', ItemCategory."Parent Category", '', CurrNode2);
                            end;

                    end;
                    AddElement(CurrNode, 'GROUP_ORDER', Format(ArticleMapOrderCounter), '', CurrNode2);
                    AddGroupNode(CurrNode, 'MIME_INFO');
                    AddGroupNode(CurrNode, 'MIME');
                    AddElement(CurrNode, 'MIME_TYPE', 'image/png', '', CurrNode2);
                    AddElement(CurrNode, 'MIME_SOURCE', ItemCategory.Code + '.png', '', CurrNode2);

                    GetParentNode(CurrNode, CurrNode2);
                    CurrNode := CurrNode2;
                    GetParentNode(CurrNode, CurrNode2);
                    CurrNode := CurrNode2;
                    GetParentNode(CurrNode, CurrNode2);
                    CurrNode := CurrNode2;
                until ItemCategory.NEXT = 0;
            end;


            GetParentNode(CurrNode, CurrNode2);
            CurrNode3 := CurrNode2;

            ArticleMapOrderCounter := 0;
            WebshopItem.SetRange("Webshop", RequestWebshopCode);
            WebshopItem.SetRange(Publish, RequestWebshopPublish);
            //WebshopItem.SetRange("Item No.", RequestItemNo);
            if WebshopItem.FindSet() then begin
                repeat
                    Item.SetFilter("No.", WebshopItem."Item No.");
                    if Item.FINDSET then begin
                        repeat
                            // Artikel werden gefüllt 
                            AddGroupNode(CurrNode2, 'ARTICLE');
                            AddAttribute(CurrNode2, 'mode', 'new');
                            CurrNode := CurrNode2;
                            AddElement(CurrNode, 'SUPPLIER_AID', Item."No.", '', CurrNode2);
                            AddGroupNode(CurrNode, 'ARTICLE_DETAILS');
                            AddElement(CurrNode, 'DESCRIPTION_SHORT', Item.Description + ' ' + Item."Description 2", '', CurrNode2);

                            ExtentedTextLines.SetRange("No.", Item."No.");
                            ExtentedTextLines.SetRange("Table Name", 2);
                            ExtentedTextLines.SetRange("Language Code", 'DEU');
                            DescLong := '';
                            if ExtentedTextLines.FindSet() then begin
                                repeat
                                    DescLong := DescLong + ' ' + ExtentedTextLines.Text;
                                until ExtentedTextLines.Next() = 0;
                            end;

                            AddElement(CurrNode, 'DESCRIPTION_LONG', DescLong, '', CurrNode2);
                            AddElement(CurrNode, 'EAN', Item.GTIN, '', CurrNode2);
                            AddElement(CurrNode, 'MANUFACTURER_AID', Item.Description, '', CurrNode2);
                            AddElement(CurrNode, 'MANUFACTURER_NAME', Item."Manufacturer Code", '', CurrNode2);
                            AddElement(CurrNode, 'DELIVERY_TIME', '', '', CurrNode2);

                            //ArtikelFeatures
                            GetParentNode(CurrNode, CurrNode2);
                            AddGroupNode(CurrNode2, 'ARTICLE_FEATURES');
                            CurrNode := CurrNode2;
                            ItemCrossReference.Reset();
                            ItemCrossReference.SetRange("Item No.", Item."No.");
                            ItemCrossReference.SetRange("Cross-Reference Type", 3);
                            if ItemCrossReference.FindSet() then begin
                                repeat
                                    AddGroupNode(CurrNode2, 'FEATURE');
                                    CurrNode := CurrNode2;
                                    if ItemCrossReference."Cross-Reference Type No." = '' then begin
                                        AddElement(CurrNode, 'REFERENCE_FEATURE_SYSTEM_NAME', 'Barcode', '', CurrNode2);
                                    end else begin
                                        AddElement(CurrNode, 'REFERENCE_FEATURE_SYSTEM_NAME', ItemCrossReference."Cross-Reference Type No.", '', CurrNode2);
                                    end;
                                    AddElement(CurrNode, 'REFERENCE_FEATURE_GROUP_ID', ItemCrossReference."Cross-Reference No.", '', CurrNode2);
                                    GetParentNode(CurrNode, CurrNode2);
                                    CurrNode := CurrNode2;
                                until ItemCrossReference.Next() = 0;
                            end;
                            AttributeValue.Reset();
                            AttributeValue.SetRange("Source Table ID", Database::Item);
                            AttributeValue.SetRange("Source Subtype", 0);
                            AttributeValue.SetRange("Source No.", Item."No.");
                            //Item.SetFilter("No.", '%1..%2|%3', '1000', '1001')
                            // AttributeValue.SetFilter("Attribute Code", '%AM%');
                            AttributeValue.SetFilter("Attribute Code", 'AM*');
                            if AttributeValue.FindSet() then begin
                                repeat
                                    iF ((AttributeValue."Attribute Code" <> 'AM-0038') and
                                       (AttributeValue."Attribute Code" <> 'AM-0039') and
                                       (AttributeValue."Attribute Code" <> 'AM-0040') and
                                        (AttributeValue."Attribute Code" <> 'AM-0041') and
                                        (AttributeValue."Attribute Code" <> 'AM-0042') and
                                        (AttributeValue."Value Text" <> '')
                                     ) then begin
                                        AddGroupNode(CurrNode2, 'FEATURE');
                                        CurrNode := CurrNode2;
                                        Attribute.Reset();
                                        Attribute.SetRange("Code", AttributeValue."Attribute Code");
                                        Attribute.FindSet();
                                        AddElement(CurrNode, 'FNAME', Attribute.Description, '', CurrNode2);
                                        AddElement(CurrNode, 'FVALUE', AttributeValue."Value Text", '', CurrNode2);
                                        GetParentNode(CurrNode, CurrNode2);
                                        CurrNode := CurrNode2;
                                    end;
                                until AttributeValue.Next() = 0;

                            end;


                            GetParentNode(CurrNode, CurrNode2);
                            AddGroupNode(CurrNode2, 'ARTICLE_ORDER_DETAILS');
                            CurrNode := CurrNode2;
                            AddElement(CurrNode, 'ORDER_UNIT', GetUnit(Item."Sales Unit of Measure"), '', CurrNode2);
                            AddElement(CurrNode, 'PRICE_QUANTITY', Format(Item."Sales Price Unit"), '', CurrNode2);
                            AddElement(CurrNode, 'QUANTITY_MIN', Format(Item."Package Unit"), '', CurrNode2);
                            AddElement(CurrNode, 'QUANTITY_INTERVAL', Item."Package Unit", '', CurrNode2);

                            GetParentNode(CurrNode, CurrNode2);

                            AddGroupNode(CurrNode2, 'ARTICLE_PRICE_DETAILS');
                            CurrNode := CurrNode2;
                            AddGroupNode(CurrNode2, 'ARTICLE_PRICE');
                            AddAttribute(CurrNode2, 'price_type', 'net_list');
                            CurrNode := CurrNode2;
                            AddElement(CurrNode, 'PRICE_AMOUNT', Format(Item."Unit Price"), '', CurrNode2);
                            AddElement(CurrNode, 'PRICE_CURRENCY', 'EUR', '', CurrNode2);

                            GetParentNode(CurrNode, CurrNode2);
                            CurrNode := CurrNode2;
                            GetParentNode(CurrNode, CurrNode2);
                            AddGroupNode(CurrNode2, 'MIME_INFO');
                            CurrNode := CurrNode2;

                            AttributeValue.Reset();
                            AttributeValue.SetRange("Source Table ID", Database::Item);
                            AttributeValue.SetRange("Source Subtype", 0);
                            AttributeValue.SetRange("Source No.", Item."No.");
                            AttributeValue.SetFilter("Attribute Code", '%1|%2|%3|%4|%5', 'AM-0038', 'AM-0039', 'AM-0040', 'AM-0041', 'AM-0042');
                            Counter := 0;
                            if AttributeValue.FindSet() then begin
                                repeat
                                    TempText := '';
                                    if Templist.Count() <> 0 then
                                        Templist.RemoveRange(1, Templist.Count());


                                    Templist := AttributeValue."Value Text".Split('.');
                                    AddGroupNode(CurrNode, 'MIME');
                                    if Templist.Count() >= 2 then begin
                                        Templist.Get(Templist.Count(), TempText);
                                        AddElement(CurrNode, 'MIME_TYPE', 'image/' + TempText, '', CurrNode2);
                                        AddElement(CurrNode, 'MIME_SOURCE', AttributeValue."Value Text", '', CurrNode2);
                                        AddElement(CurrNode, 'MIME_ORDER', Format(Counter), '', CurrNode2);
                                        Counter += 1;
                                    end;
                                    /* else begin
                                        AddElement(CurrNode, 'MIME_TYPE', '', '', CurrNode2); //TODO: Standard PNG ? 
                                        AddElement(CurrNode, 'MIME_SOURCE', '', '', CurrNode2);
                                        AddElement(CurrNode, 'MIME_ORDER', '', '', CurrNode2);
                                    end; */ //TODO: bei Leerem wert nichts definiert
                                    GetParentNode(CurrNode, CurrNode2);
                                    CurrNode := CurrNode2;
                                until AttributeValue.Next() = 0;

                            end;
                            GetParentNode(CurrNode, CurrNode2);
                            CurrNode := CurrNode2;
                            GetParentNode(CurrNode, CurrNode2);
                            AddGroupNode(CurrNode2, 'ARTICLE_TO_CATALOGGROUP_MAP');
                            CurrNode := CurrNode2;
                            AddElement(CurrNode, 'ART_ID', Item."No.", '', CurrNode2);
                            AddElement(CurrNode, 'CATALOG_GROUP_ID', Item."Item Category Code", '', CurrNode2);
                            AddElement(CurrNode, 'ARTICLE_TO_CATALOGGROUP_MAP_ORDER', Format(ArticleMapOrderCounter), '', CurrNode2);
                            GetParentNode(CurrNode, CurrNode2);
                            CurrNode3 := CurrNode2;
                            ArticleMapOrderCounter += 1;
                        until Item.NEXT() = 0;
                    end;
                until WebshopItem.Next() = 0;
            end;
            TextDoc := '';
            GetRootNode(XMLDoc, CurrNode);

            //XMLWriteOpt.PreserveWhitespace(true);
            //XMLDoc.WriteTo(XMLWriteOpt, TextDoc);
            MemStream := MemStream.MemoryStream();
            xmldoc.WriteTo(MemStream);
            //XMLDoc.
            //TextDoc := DelChr(TextDoc, '=', '0x00');


            //MemStream.WriteTo(TextDoc)
            TempBlob.Blob.CreateInStream(IStream, TextEncoding::UTF8);
            MemStream.WriteTo(IStream);
            MemStream.Close();
            //TempBlob.Blob.CREATEOUTSTREAM(OStream, TextEncoding::UTF8);
            //OStream.Write(TextDoc, StrLen(TextDoc));
            FileName := 'BMECATexport' + Format(CURRENTDATETIME(), 0, '<Day,2><Month,2><Year4>_<Hour,2><Minute,2><Second,2>') + '.xml';
            FilePath := FileManagement.ServerTempFileName('XML');
            FilePath := FileManagement.GetDirectoryName(FilePath) + '\' + FileName;

            if FileManagement.DeleteServerFile(FilePath) then;
            FileManagement.BLOBExportToServerFile(TempBlob, FilePath);

            FileTransportManagment.SetGlobalsForFTP('FTPBMECATEXPORT', FileName, FilePath, false);
            FileTransportManagment.Run();

            if FileManagement.DeleteServerFile(FilePath) then;
        end;

        // File Transport type;

    end;



    Var
        TextDoc: Text;
        FileName: Text;
        FilePath: Text;
        TestFile: File;

        MemStream: DotNet MemoryStream;

    local procedure GetUnit(Unit: Text): Text
    begin
        case Unit of
            'STÜCK':
                begin
                    Unit := 'C62';
                end;
            'KG':
                begin
                    Unit := 'KGM';
                end;
            'PAAR':
                begin
                    Unit := 'PR';
                end;
            'SATZ':
                begin
                    Unit := 'SET';
                end;
            'KILOMETER':
                begin
                    Unit := 'KMT';
                end;
            'L':
                begin
                    Unit := 'LTR';
                end;
            'M':
                begin
                    Unit := 'MTR';
                end;
            'M2':
                begin
                    Unit := 'MTK';
                end;
            'M3':
                begin
                    Unit := 'MTQ';
                end;
            'MM':
                begin
                    Unit := 'MMT';
                end;
            'PAKET':
                begin
                    Unit := 'PK';
                end;
            'PALETTE':
                begin
                    Unit := 'PF';
                end;
            'SCHACHTEL':
                begin
                    Unit := 'SCH';
                end;
            'STUNDE':
                begin
                    Unit := 'HUR';
                end;
            'TAG':
                begin
                    Unit := 'DAY';
                end;
            else begin
                    Unit := '';
                end;
        end;

        exit(Unit);
    end;


}
