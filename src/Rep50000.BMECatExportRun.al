report 50000 "BMECatExport Run"
{
    ProcessingOnly = true;

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Opt)
                {

                    Caption = 'Parameter setzen';
                    field("Webshop"; RequestWebshopCode)
                    {
                        TableRelation = "Webshop - Item";
                        Caption = 'Webshop Code', Comment = 'ENU="Webshop Code";DEU="Webshop Code"';
                    }
                    field("Publish"; RequestWebshopPublish)
                    {
                        TableRelation = "Webshop - Item";
                        Caption = 'Publish', Comment = 'ENU="Publish";DEU="Veröffentlicht"';
                    }
                    //field("No."; RequestItemNo)
                    //{
                    //  TableRelation = "Item";
                    //  Caption = 'Item No.', Comment = 'ENU="Item No.";DEU="Artikelnummer"';
                    //}



                }
            }
        }

    }
    trigger OnPreReport()
    var
        BMECATmgt: Codeunit XML_BMECATMgt;
    begin
        BMECATmgt.ExportBMECAT(RequestWebshopCode, RequestWebshopPublish, RequestItemNo);
    end;

    var
        RequestWebshopCode: Code[250];
        RequestWebshopPublish: Boolean;
        RequestItemNo: Code[50];

}
